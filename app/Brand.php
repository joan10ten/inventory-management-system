<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'name',
        'logo',
        'description',
        'est',
        'supplier_id',
        'product_id',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot()
	{
		parent::boot();

		static::deleting(function ($brand) { // before delete() method call this
			$brand->products()->delete();
			// do the rest of the cleanup...
		});
	}
}
