<?php

namespace App;

class Common
{
	public static $categories = [
		'01' => 'Beverages',
		'02' => 'Bread/Bakery',
		'03' => 'Cleaners',
		'04' => 'Canned/Jarred Goods',
		'05' => 'Dairy',
		'06' => 'Dry/Baking Goods',
		'07' => 'Frozen Foods',
		'07' => 'Paper Goods',
		'08' => 'Personal Care',
		'09' => 'Electronics',
		'10' => 'Clothing',
		'11' => 'Others',
	];
}
