<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Supplier;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::oldest()->paginate(5);

        return view('brands.index', compact('brands'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all();
        return view('brands.create', compact('suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'logo' => 'mimes:jpeg,jpg,png|required|image|max:600',
            'description' => 'required|max:200',
            'est' => 'required|numeric',
            'supplier_id' => 'required|numeric',
            // 'product_id' => 'required|numeric',
        ]);

        $logo = $request->file('logo');
        $imagedata = file_get_contents($logo);
        $base64 = base64_encode($imagedata);

        $brand = new Brand();
        $brand->name = $request->name;
        $brand->logo = $base64;
        $brand->description = $request->description;
        $brand->est = $request->est;
        $brand->supplier_id = $request->supplier_id;
        $brand->user_id = $request->user()->id;
        // $brand->product_id = $request->product_id;

        $brand->save();

        // Brand::create($request->all());

        return redirect()->route('brands.index')
            ->with('success', 'Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('brands.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-brand', $brand);

        $suppliers = Supplier::all();
        return view('brands.edit', compact('brand', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-brand', $brand);

        $request->validate([
            'name' => 'required|max:100',
            'logo' => 'mimes:jpeg,jpg,png|image|max:600',
            'description' => 'required|max:200',
            'est' => 'required|numeric',
            'supplier_id' => 'required|numeric',
            // 'product_id' => 'required|numeric',
        ]);

        if ($request->file('logo') != null) {
            $logo = $request->file('logo');
            $imagedata = file_get_contents($logo);
            $base64 = base64_encode($imagedata);

            $brand->name = $request->name;
            $brand->logo = $base64;
            $brand->description = $request->description;
            $brand->est = $request->est;
            $brand->supplier_id = $request->supplier_id;


            $brand->update();
        } else {
            $brand->update($request->all());
        }



        // $brand->product_id = $request->product_id;



        return redirect()->route('brands.index')
            ->with('success', 'Brand updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('delete-brand', $brand);

        $brand->delete();

        return redirect()->route('brands.index')
            ->with('success', 'Brand deleted successfully');
    }
    public function gallery()
    {
        $brands = Brand::oldest()->paginate();

        return view('brands.gallery', compact('brands'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    
}
