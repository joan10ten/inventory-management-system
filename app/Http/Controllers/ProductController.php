<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Product;
use App\Brand;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::oldest()->paginate(5);

        return view('products.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();

        return view('products.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'brand_id' => 'required|numeric',
            'description' => 'required|max:200',
            'category' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|image|max:600',
            // 'supplier_id' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        $image = $request->file('image');
        $imagedata = file_get_contents($image);
        $base64 = base64_encode($imagedata);

        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->category = $request->category;
        $product->image = $base64;
        // $product->supplier_id = $request->supplier_id;
        $product->user_id = $request->user()->id;
        $product->quantity = $request->quantity;

        $product->save();

        // Product::create($request->all());

        return redirect()->route('products.index')
            ->with('success', 'Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-product', $product);

        $brands = Brand::all();
        return view('products.edit', compact('product', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-product', $product);

        $request->validate([
            'name' => 'required|max:100',
            'brand_id' => 'required|numeric',
            'description' => 'required|max:200',
            'category' => 'required',
            'image' => 'mimes:jpeg,jpg,png|image|max:600',
            // 'supplier_id' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        if ($request->file('image') != null) {

            $image = $request->file('image');
            $imagedata = file_get_contents($image);
            $base64 = base64_encode($imagedata);

            $product->name = $request->name;
            $product->description = $request->description;
            $product->category = $request->category;
            $product->brand_id = $request->brand_id;
            $product->image = $base64;
            // $product->supplier_id = $request->supplier_id;
            $product->quantity = $request->quantity;

            $product->update();
        } else {
            $product->update($request->all());
        }


        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('delete-product', $product);

        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Product deleted successfully');
    }

    public function gallery()
    {
        $products = Product::oldest()->paginate();

        return view('products.gallery', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function galleryindex()
    {
        $products = Product::oldest()->paginate();
        $brands = Brand::oldest()->paginate();


        return view('gallery.index', compact('products','brands'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
