<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Supplier;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::oldest()->paginate(5);


        return view('suppliers.index', compact('suppliers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'company' => 'required|max:100',
            'address' => 'required|max:200',
            // 'brand_id' => 'required|numeric',
            // 'product_id' => 'required',
        ]);
        $supplier = new Supplier();
        $supplier->fill($request->all());
        $supplier->user_id = $request->user()->id;
        $supplier->save();

        return redirect()->route('suppliers.index')
            ->with('success', 'Supplier created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return view('suppliers.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-supplier', $supplier);

        return view('suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('update-supplier', $supplier);

        $request->validate([
            'name' => 'required|max:100',
            'company' => 'required|max:100',
            'address' => 'required|max:200',
            // 'brand_id' => 'required|numeric',
            // 'product_id' => 'required',
        ]);


        $supplier->update($request->all());

        return redirect()->route('suppliers.index')
            ->with('success', 'Supplier updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        if (!Gate::allows('manage-inventory'))
            $this->authorize('delete-supplier', $supplier);

        $supplier->delete();

        return redirect()->route('suppliers.index')
            ->with('success', 'Supplier deleted successfully');
    }
}
