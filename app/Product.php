<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
    'name',
    'brand_id',
    'description',
    'category',
    'image',
    'quantity',
    'supplier_id',
  ];

  public function brand()
  {
    return $this->belongsTo(Brand::class);
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  // public function supplier()
  // {
  //   return $this->belongsTo(Supplier::class);
  // }
}
