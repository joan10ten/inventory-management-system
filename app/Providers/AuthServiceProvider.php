<?php

namespace App\Providers;

use App\Policies\BrandPolicy;
use App\Policies\ProductPolicy;
use App\Policies\SupplierPolicy;
use App\Supplier;
use App\Product;
use App\Brand;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user) {
            if ($user->role == 'superAdmin') {
                return true;
            }
        });

        // /* define a admin user role */
        // Gate::define('isSuperAdmin', function ($user) {
        //     return $user->role == 'superAdmin';
        // });

        // /* define a manager user role */
        // Gate::define('isAdmin', function ($user) {
        //     return $user->role == 'admin';
        // });

        // /* define a user role */
        // Gate::define('isMember', function ($user) {
        //     return $user->role == 'member';
        // });

        Gate::define('manage-users', function ($user) {
            return $user->role == 'superAdmin';
        });

        Gate::define('manage-inventory', function ($user) {
            return $user->role == 'admin';
        });

        Gate::define('update-product', function ($user, $product) {
            return $user->id == $product->user_id;
        });
        Gate::define('delete-product', function ($user, $product) {
            return $user->id == $product->user_id;
        });
        Gate::define('update-brand', function ($user, $brand) {
            return $user->id == $brand->user_id;
        });
        Gate::define('delete-brand', function ($user, $brand) {
            return $user->id == $brand->user_id;
        });
        Gate::define('update-supplier', function ($user, $supplier) {
            return $user->id == $supplier->user_id;
        });
        Gate::define('delete-supplier', function ($user, $supplier) {
            return $user->id == $supplier->user_id;
        });
        //
    }
}
