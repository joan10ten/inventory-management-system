<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
	protected $fillable = [
		'name',
		'company',
		'address',
		// 'brand_id',
		// 'product_id'
	];

	// public function products()
	// {
	// 	return $this->hasMany(Product::class);
	// }

	public function brands()
	{
		return $this->hasMany(Brand::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public static function boot()
	{
		parent::boot();

		static::deleting(function ($supplier) { // before delete() method call this
			foreach ($supplier->brands as $brand) {
				$brand->products()->delete();
			}
			$supplier->brands()->delete();

			// do the rest of the cleanup...
		});
	}
}
