@extends('layouts.main')

@section('content')
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<style>
	.btn.btn-back {
		margin-top: 20px;
		margin-bottom: 15px;
		margin-left: 20px;
	}

	.header-text {
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 5px;
		font-size: 30px;
		font-family: 'Raleway', sans-serif;
		font-weight: bold;
	}

	.row.input-pic {
		margin-left: 8px;
		margin-bottom: 30px
	}

	.btn-add {
		margin-left: 13px;
		margin-bottom: 40px;
	}

	hr {
        margin-bottom: 20px;
    }

    .req-text {
        margin-bottom: 30px;
    }

</style>
</head>

<div class="container">
    <div class="row">
		<a class="btn btn-back btn-primary" href="{{ route('brands.index') }}"> Back</a>
	</div>

	<div class="row">
		<h3 class="header-text"> Add New Brand </h3>
	</div>

	<hr>

    <div class="req-text">
        <text>Photo size of 500x500 is recommended and file size of less than 600kb is required.</text>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('brands.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

        <div class="row input-pic">
            <input type='file' onchange="readURL(this);" name="logo" accept="image/png, image/jpeg, image/jpg" required focus/>
            <img id="blah" src="#" alt="your logo" />
			<span class="text-danger">{{ $errors->first('logo') }}</span>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-md-6">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name" required focus>
				<span class="text-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Description:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Description" required focus></textarea>
				<span class="text-danger">{{ $errors->first('description') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Estalished Since:</strong>
                <input type="number" name="est" class="form-control" min="1000" max="2020" step="1" value="2020" placeholder="Year of Establish" required focus>
				<span class="text-danger">{{ $errors->first('est') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Supplier:</strong>
                <select class="form-control" name="supplier_id" required focus>
                    <option value="" disabled selected>Please select supplier</option>
                    @foreach($suppliers as $supplier)
                        <option value="{{$supplier->id}}">{{ $supplier->name }} </option>
                    @endforeach
                </select>
				<span class="text-danger">{{ $errors->first('supplier_id') }}</span>
            </div>
            
            <button type="submit" class="btn btn-add btn-success col-sm-2">Add</button>
        </div>
    </form>
</div>

@endsection
