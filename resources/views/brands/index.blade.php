@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

    <style>
        .btn.btn-add {
            margin-top: 10px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .btn.btn-action {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-left: 20px;
            margin-right: 10px;
        }

        .header-text {
            margin-top: 20px;
            margin-left: 35px;
            font-size: 25px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .table {
            margin-left: 20px;
            margin-right: 0;
        }

        th {
            font-family: 'Raleway', sans-serif;
        }

        td {
            word-wrap: break-word;
        }

        .alert {
            margin-left: 20px;
            max-height: 50px;
        }
    </style>
</head>

<div class="row">
    <h3 class="header-text"> Manage Brands </h3>
</div>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-add btn-success" href="{{ route('brands.create') }}"> + Add New Brand</a>
            <a class="btn btn-add btn-success" href="{{ route('brands.gallery') }}">Brands Photo Gallery</a>

        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Logo</th>
            <th>Description</th>
            <th>Established Since</th>
            <th>Supplier</th>
            <th>Item(s)</th>
            <th>Action</th>
        </tr>
    </thead>

    </tbody>
    @foreach ($brands as $brand)

    <tr>
        <td width="1%">{{ ++$i }}</td>
        <td width="15%">{{ $brand->name }}</td>
        <td width="10%"><img src="data:image/jpeg;base64,{{$brand->logo}}" width="150" height="150" /></td>
        <td width="30%">{{ $brand->description }}</td>
        <td width="10%">{{ $brand->est }}</td>
        <td width="15%"><a href={{ route('suppliers.show',$brand->supplier->id) }}>{{ $brand->supplier->name }}</a></td>
        <td width="15%">@foreach ( $brand->products as $product) 
            <li>
                <a href={{ route('products.show',$product->id) }}>{{$product->name}}</a> 
            </li>
            @endforeach
        </td>
        <td width="10%">
            <form action="{{ route('brands.destroy',$brand->id) }}" method="POST">
                <div class="row">
                    <a class="btn btn-action btn-primary" href="{{ route('brands.show',$brand->id) }}">Show</a>
                </div>
                @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('update-brand', $brand))
                <div class="row">
                    <a class="btn btn-action btn-warning" href="{{ route('brands.edit',$brand->id) }}">Edit</a>
                </div>
                @endif
                @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('delete-brand', $brand))
                <div class="row">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-action btn-danger" onclick="return confirm('Are you sure? All associated products with this brand will be removed.')">Delete</button>
                </div>
                @endif
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

{!! $brands->links() !!}

@endsection
