@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .row.pic {
            margin-left: 8px;
            margin-bottom: 30px
        }

        .btn-add {
            margin-left: 13px;
            margin-bottom: 40px;
        }

        hr {
            margin-bottom: 20px;
        }

        label {
            display: inline-block;
            width: 200px;
            text-align: left;
            font-size: 17px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .desc {
            padding-left: 124px;
        }

        .container {
            margin-bottom: 40px;
        }
    </style>
    <div class="container">
        <div class="row">
            <a class="btn btn-back btn-primary" href="{{ route('brands.index') }}"> Back</a>
        </div>

        <div class="row">
            <h3 class="header-text"> Show Brand </h3>
        </div>

        <hr>

        <div class="row pic">
            <img id="blah" src="data:image/jpeg;base64,{{$brand->logo}}" width="300" height="300" />
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label>Name</label>
                {{ $brand->name }}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-1">
                <label>Description</label>
            </div>
            <div class="col-md desc">
                {{ $brand->description }}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label>Established Since</label>
                {{ $brand->est }}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label>Supplier</label>
                <a href={{route('suppliers.show',$brand->supplier->id)}}>{{ $brand->supplier->name }}</a>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label>Item(s)</label>
                @foreach ( $brand->products as $product) <li><a href={{ route('products.show',$product->id) }}>{{$product->name}}</a></li> @endforeach
            </div>
        </div>

    </div>
    </div>
    @endsection