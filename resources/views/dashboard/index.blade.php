@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

    <style>
        .btn.btn-add {
            margin-top: 10px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .btn.btn-action {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 20px;
            margin-left: 35px;
            font-size: 25px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .card-row {
            margin-left: 20px;
            margin-top: 20px;
        }

        .card {
            height: 400px;
        }

        .card-title {
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
            font-size: 30px;
        }

        .card-logo {
            width: 60px;
            height: 60px;
            margin-top: 10px;
        }

        p {
            margin-top: 160px;
        }

        .card-text {
            font-family: 'Raleway', sans-serif;
            font-size: 70px;
            text-align: right;
        }

        hr{
            margin-left: 13px;
        }

        a:hover {
            text-decoration: none;
        }

        .card:hover {
            filter: opacity(60%);
        }

        .card-auth:hover{
            filter: none;
        }

    </style>
</head>

<div class="row">
    <h3 class="header-text"> Dashboard </h3>
</div>

<hr>

<div class="row card-row">
    <div class="col">
        @can('manage-users')
        <a href="/users">
            <div class="card card-user text-white bg-danger">
                <div class="card-body">
                    <h5 class="card-title">Total Users</h5>
                    <img class="card-logo" src="/img/user.svg">
                    <p class="card-text">{{ $userCount }}</p>

                </div>
            </div>
        </a>
        @else
        <div class="card card-auth text-white bg-danger">
            <div class="card-body">
                <h5 class="card-title">Total Users</h5>
                <img class="card-logo" src="/img/user.svg">
                <p class="card-text">{{ $userCount }}</p>
            </div>
        </div>
        @endcan
    </div>

    <div class="col">
        <a href="/suppliers">
            <div class="card text-white bg-warning">
                <div class="card-body">
                    <h5 class="card-title">Total Suppliers</h5>
                    <img class="card-logo" src="/img/supplier.svg">
                    <p class="card-text">{{ $supplierCount }}</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col">
        <a href="/brands">
            <div class="card text-white bg-primary">
                <div class="card-body">
                    <h5 class="card-title">Total Brands</h5>
                    <img class="card-logo" src="/img/branding.svg">
                    <p class="card-text">{{ $brandCount }}</p>
                </div>

            </div>
        </a>
    </div>

    <div class="col">
        <a href="/products">
            <div class="card text-white bg-success">
                <div class="card-body">
                    <h5 class="card-title">Total Products</h5>
                    <img class="card-logo" src="/img/product.svg">
                    <p class="card-text">{{ $productCount }}</p>
                </div>
            </div>
        </a>
    </div>
</div>



@endsection