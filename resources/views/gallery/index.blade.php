@extends('layouts.main')

@section('content')

<head>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function() {

          $('.image-popup-vertical-fit').magnificPopup({
              type: 'image',
              closeOnContentClick: true,
              mainClass: 'mfp-img-mobile',
              image: {
                  verticalFit: true
              }

          });

          $('.image-popup-fit-width').magnificPopup({
              type: 'image',
              closeOnContentClick: true,
              image: {
                  verticalFit: false
              }
          });

          $('.image-popup-no-margins').magnificPopup({
              type: 'image',
              closeOnContentClick: true,
              closeBtnInside: false,
              fixedContentPos: true,
              mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
              image: {
                  verticalFit: true
              },
              zoom: {
                  enabled: true,
                  duration: 300 // don't foget to change the duration also in CSS
              }
          });

      });
  </script>
  <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
  <style>
      .row-pic {
          margin-top: 20px;
          margin-left: 100px;
      }

      .row.pic {
          margin-bottom: 10px;
          display: table;
      }

      .text {
          text-align: center;
      }

      hr{
            margin-left: 13px;
        }

      .picturecontainer {
          display: block;
          margin: 20px;
      }

      .header-text {
          margin-top: 20px;
          margin-left: 35px;
          font-size: 25px;
          font-family: 'Raleway', sans-serif;
          font-weight: bold;
      }

      .header {
          margin-left: 35px;
          font-size: 25px;
          font-family: 'Raleway', sans-serif;
          font-weight: bold;
      }

      .image:hover {
          filter: opacity(60%);
      }

      /* padding-bottom and top for image */
      .mfp-no-margins img.mfp-img {
          padding: 0;
          height: 500px;
          width: 500px;
      }

      /* position of shadow behind the image */
      .mfp-no-margins .mfp-figure:after {
          top: 0;
          bottom: 0;
      }

      /* padding for main container */
      .mfp-no-margins .mfp-container {
          padding: 0;
      }

      .mfp-with-zoom .mfp-container,
      .mfp-with-zoom.mfp-bg {
          opacity: 0;
          -webkit-backface-visibility: hidden;
          -webkit-transition: all 0.3s ease-out;
          -moz-transition: all 0.3s ease-out;
          -o-transition: all 0.3s ease-out;
          transition: all 0.3s ease-out;
      }

      .mfp-with-zoom.mfp-ready .mfp-container {
          opacity: 1;
      }

      .mfp-with-zoom.mfp-ready.mfp-bg {
          opacity: 0.8;
      }

      .mfp-with-zoom.mfp-removing .mfp-container,
      .mfp-with-zoom.mfp-removing.mfp-bg {
          opacity: 0;
      }
  </style>
</head>

<div class="row">
    <h3 class="header-text"> Photo Gallery </h3>
</div>

<hr> 

<div class="row">
      <h3 class ="header"> Products </h3>
</div>

<div class="row row-pic">
    @foreach($products as $product)
    <div class="picturecontainer">
        <a class="image-popup-no-margins image" href="data:image/jpeg;base64,{{$product->image}}"><img src="data:image/jpeg;base64,{{$product->image}}"  width="300" height="300"></a>
        <br>
        <a href="{{ route('products.show',$product->id) }} ">
            <center><strong>{{$product->name}}</strong></center>
        </a>
    </div>
    @endforeach
</div>

<hr>

<div class="row">
      <h3 class ="header"> Brands </h3>
</div>

<div class="row row-pic">
    @foreach($brands as $brand)
    <div class="picturecontainer">
        <a class="image-popup-no-margins image" href="data:image/jpeg;base64,{{$brand->logo}}"><img src="data:image/jpeg;base64,{{$brand->logo}}"  width="300" height="300"></a>
        <br>
        <a href="{{ route('brands.show',$brand->id) }} ">
            <center><strong>{{$brand->name}}</strong></center>
        </a>
    </div>
    @endforeach
</div>

@endsection
