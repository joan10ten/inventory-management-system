<!DOCTYPE html>
<html lang="en">

<head>
	<title>Inventory Management System</title>
	<!-- CSS And JavaScript -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
	<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
	<style>
		.sidenav {
			height: 100%;
			width: 200px;
			position: fixed;
			top: 20;
			left: 0;
			background-color: #1d2a3b;
			padding-top: 20px;
		}

		.sidenav a {
			padding: 15px 8px 15px 32px;
			text-decoration: none;
			font-size: 20px;
			color: #fafcff;
			display: block;
			transition: 0.3s;
		}

		.sidenav a:hover {
			background-color: #9f2cbf;
			color: #f1f1f1;
		}

		.text-light {
			padding-left: 10px;
			font-size: 20px;
		}

		.nav-logo {
			margin-left: 10px;
		}

		.logo-sidenav-dashboard {
			width: 30px;
			height: 30px;
			margin-right: 10px;
		}

		div.content {
			width: 100%;
			height: 100%;
			padding-right: 60px;
			padding-left: 205px;
		}

		.user-type {
			margin-right: 15px;
			padding-top: 3px;
			font-family: 'Raleway', sans-serif;
			font-size: 22px;
		}

		.logout {
			margin-right: 5px;
		}

		.login {
			margin-right: 10px;
		}
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-info sticky-top">
		<a href="/" class="navbar-brand">
			<img class=nav-logo src="/img/box.svg" width="40" height="40" alt="">
			<span class=text-light> Inventory Management System </span>
		</a>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Right Side Of Navbar -->
			<ul class="navbar-nav ml-auto">
				<!-- Authentication Links -->
				@guest
				<li class="nav-item">
					<button type="button" class="btn btn-warning login" href="{{ route('login') }}">{{ __('Login') }}</button>
				</li>
				@if (Route::has('register'))
				<li class="nav-item">
					<button type="button" class="btn btn-outline-light" href="{{ route('register') }}">{{ __('Register') }}</button>
				</li>
				@endif
				@else
				<li class="nav-item text-light user-type">
					<a class="user-type">
						{{ Auth::user()->name }}
					</a>
				</li>
				<li>
					<button type="button" class="btn btn-warning logout" href="{{ route('logout') }}" onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
						{{ __('Logout') }}
					</button>
				</li>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
				@endguest
			</ul>
		</div>
	</nav>
	
	<div class="col">
		<div class="sidenav">
			<a href="/"><img class="logo-sidenav-dashboard" src="/img/dashboard.svg">Dashboard</a>
			@can('manage-users')
			<a href="/users"><img class="logo-sidenav-dashboard" src="/img/user.svg">Users</a>
			@endcan
			<a href="/suppliers"><img class="logo-sidenav-dashboard" src="/img/supplier.svg">Suppliers</a>
			<a href="/brands"><img class="logo-sidenav-dashboard" src="/img/branding.svg">Brands</a>
			<a href="/products"><img class="logo-sidenav-dashboard" src="/img/product.svg">Products</a>
			<a href="/gallery"><img class="logo-sidenav-dashboard" src="/img/image.svg">Gallery</a>
		</div>
	</div>

	<div class="content">
		@yield('content')
	</div>
</body>

</html>
