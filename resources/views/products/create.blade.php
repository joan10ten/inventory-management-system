<?php
use App\Common;
?>
@extends('layouts.main')

@section('content')
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<style>
	.btn.btn-back {
		margin-top: 20px;
		margin-bottom: 15px;
		margin-left: 20px;
	}

	.header-text {
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 5px;
		font-size: 30px;
		font-family: 'Raleway', sans-serif;
		font-weight: bold;
	}

	.row.input-pic {
		margin-left: 8px;
		margin-bottom: 30px
	}

	.btn-add {
		margin-left: 13px;
		margin-bottom: 40px;
	}

	hr {
        margin-bottom: 20px;
    }

	.req-text {
        margin-bottom: 30px;
    }

</style>
</head>

<div class="container">
	<div class="row">
		<a class="btn btn-back btn-primary" href="{{ route('products.index') }}"> Back</a>
	</div>

	<div class="row">
		<h3 class="header-text"> Add New Product </h3>
	</div>

	<hr>

	<div class="req-text">
        <text>Photo size of 500x500 is recommended and file size of less than 600kb is required.</text>
    </div>

	@if ($errors->any())
	<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
			</ul>
	</div>
	@endif

	<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
	@csrf

		<div class="row input-pic">
			<input type='file' onchange="readURL(this);" name="image" accept="image/png, image/jpeg" required focus/>
			<img id="blah" src="#" alt="your image" width="200" height="200"/>
			<span class="text-danger">{{ $errors->first('image') }}</span>
		</div>

		<hr>

		<div class="row">
			<div class="form-group col-md-6">
				<strong>Name:</strong>
				<input type="text" name="name" class="form-control" placeholder="Name" required focus>
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div>

			<div class="form-group col-md-12">
				<strong>Brand:</strong>
				<select class="form-control" name="brand_id" required focus>
                    <option value="" disabled selected>Please select Brand</option>
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{ $brand->name }}</option>
                    @endforeach
                </select>
				<span class="text-danger">{{ $errors->first('brand_id') }}</span>
			</div>

			<!-- <div class="form-group col-md-12">
				<strong>Supplier:</strong>
				<input type="number" name="supplier_id" class="form-control" placeholder="Supplier">
			</div> -->

			<div class="form-group col-md-12">
				<strong>Description:</strong>
				<textarea class="form-control" style="height:150px" name="description" placeholder="Description" required focus></textarea>
				<span class="text-danger">{{ $errors->first('description') }}</span>
			</div>

			<div class="form-group col-md-12">
				<strong>Category:</strong>
				<select class="form-control" name="category" required focus>

                    <option value="" disabled selected>Please select category</option>
                    @foreach(Common::$categories as $category)
                        <option value="{{$category}}">{{ $category }}</option>
                    @endforeach
                </select>
				<span class="text-danger">{{ $errors->first('category') }}</span>
			</div>

			<div class="form-group col-md-12">
				<strong>Quantity:</strong>
				<input type="number" name="quantity" class="form-control" placeholder="Quantity" required focus>
				<span class="text-danger">{{ $errors->first('quantity') }}</span>
			</div>

			<button type="submit" class="btn btn-add btn-success col-sm-2">Add</button>
		</div>
	</form>
</div>

@endsection
