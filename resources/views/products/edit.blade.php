<?php

use App\Common;
?>
@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .row.input-pic {
            margin-left: 8px;
            margin-bottom: 30px
        }

        .btn-update {
            margin-left: 13px;
            margin-bottom: 40px;
        }

        hr {
            margin-bottom: 20px;
        }
    </style>
</head>

<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('products.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Edit Product </h3>
    </div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row input-pic">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <!-- <label for="img">Select image:</label>
                    <input type="file" id="img" name="image" accept="image/*"> -->
                    <input type='file' onchange="readURL(this);" name="image" accept="image/png, image/jpeg" />
                    <img id="blah" src="data:image/jpeg;base64,{{$product->image}}" width="200" height="200" />
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <strong>Name:</strong>
                <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Name">
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Brand:</strong>
                <select class="form-control" name="brand_id" required focus>
                    <option value="{{$product->brand_id}}" selected="{{$product->brand_id}}">{{ $product->brand->name }} - {{$product->brand_id}}</option>
                    @foreach($brands as $brand)
                    @if($brand->id == $product->brand_id)
                    @continue;
                    @endif
                    <option value="{{$brand -> id}}">{{ $brand->name }} - {{ $brand->id }}</option>
                    @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('brand_id') }}</span>
            </div>

            <!-- <div class="form-group col-md-12">
                <strong>Supplier ID:</strong>
                <input type="number" name="supplier_id" value="{{ $product->supplier_id }}" class="form-control" placeholder="Supplier ID">
            </div> -->

            <div class="form-group col-md-12">
                <strong>Description:</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Description">{{ $product->description }}</textarea>
                <span class="text-danger">{{ $errors->first('description') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Category:</strong>
                <select class="form-control" name="category" required focus>
                    <option value="{{$product->category}}" selected="{{$product->category}}">{{$product->category}}</option>
                    @foreach(Common::$categories as $category)
                    @if($category == $product->category)
                    @continue;
                    @endif
                    <option value="{{$category}}">{{ $category }}</option>
                    @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('category') }}</span>
            </div>

            <div class="form-group col-md-12">
                <strong>Quantity:</strong>
                <input type="number" name="quantity" value="{{ $product->quantity }}" class="form-control" placeholder="quantity">
                <span class="text-danger">{{ $errors->first('quantity') }}</span>
            </div>

            <button type="submit" class="btn btn-update btn-success col-sm-2">Update</button>
        </div>
    </form>
</div>
@endsection