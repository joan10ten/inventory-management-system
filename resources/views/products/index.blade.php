@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

    <style>
        .btn.btn-add {
            margin-top: 10px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .btn.btn-action {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 20px;
            margin-left: 35px;
            font-size: 25px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .table {
            margin-left: 20px;
            margin-right: 0;
        }

        th {
            font-family: 'Raleway', sans-serif;
        }

        td {
            word-wrap: break-word;
        }

        .alert {
            margin-left: 20px;
            max-height: 50px;
        }
    </style>
</head>

<div class="row">
    <h3 class="header-text"> Manage Products </h3>
</div>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-add btn-success" href="{{ route('products.create') }}"> + Add New Product</a>
              <a class="btn btn-add btn-success" href="{{ route('products.gallery') }}">Products Photo Gallery</a>
        </div>
    </div>

</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <thead>
        <tr>
            <th width="5px">#</th>
            <th>Name</th>
            <th>Brand</th>
            <th>Supplier</th>
            <th>Description</th>
            <th>Category</th>
            <th>Quantity</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($products as $product)
        <tr>
            <td width="1%">{{ ++$i }}</td>
            <td width="8%">{{ $product->name }}</td>
            <td width="8%"><a href={{ route('brands.show',$product->brand->id) }}>{{ $product->brand -> name }}</a></td>
            <td width="8%"><a href={{ route('suppliers.show',$product->brand->supplier->id) }}>{{$product->brand -> supplier-> name}}</a></td>
            <td width="12%">{{ $product->description }}</td>
            <td width="8%">{{ $product->category }}</td>
            <td width="1%">{{ $product->quantity }}</td>
            <td width="1%"><img src="data:image/jpeg;base64,{{$product->image}}" width="150px" height="150px" /></td>
            <td width="2%">
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    <div class="row">
                        <a class="btn btn-action btn-primary" href="{{ route('products.show',$product->id) }}">Show</a>
                    </div>
                    @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('update-product', $product))
                    <div class="row">
                        <a class="btn btn-action btn-warning" href="{{ route('products.edit',$product->id) }}">Edit</a>
                    </div>
                    @endif
                    @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('delete-product', $product))
                    <div class="row">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-action btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                    </div>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $products->links() !!}

@endsection
