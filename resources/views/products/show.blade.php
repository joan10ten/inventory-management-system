@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .row.pic {
            margin-left: 8px;
            margin-bottom: 30px
        }

        hr {
            margin-bottom: 20px;
        }

        label {
            display: inline-block;
            width: 140px;
            text-align: left;
            font-size: 17px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .desc {
            padding-left: 63px;
        }

        .container {
            margin-bottom: 40px;
        }
    </style>
</head>
<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('products.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Show Product </h3>
    </div>

    <hr>

    <div class="row pic">
        <img id="blah" src="data:image/jpeg;base64,{{$product->image}}" width="300" height="300" />
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Name</label>
            {{ $product->name }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Brand</label>
            <a href={{route('brands.show',$product->brand -> id)}}>{{ $product->brand -> name }}</a>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Supplier</label>
            <a href={{route('suppliers.show',$product->brand -> supplier -> id)}}>{{ $product->brand -> supplier -> name }}</a>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-1">
            <label>Description</label>
        </div>
        <div class="col-md desc">
            {{ $product->description }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Category</label>
            {{ $product->category }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Quantity</label>
            {{ $product->quantity }}
        </div>
    </div>
</div>
@endsection