
@extends('layouts.main')

@section('content')
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
<style>
	.btn.btn-back {
		margin-top: 20px;
		margin-bottom: 15px;
		margin-left: 20px;
	}
	
	.header-text {
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 5px;
		font-size: 30px;
		font-family: 'Raleway', sans-serif;
		font-weight: bold;
	}

	.btn-add { 
		margin-left: 13px;
		margin-bottom: 40px;
	}

	hr { 
        margin-bottom: 20px;
    }

</style>
</head>

<div class="container">
    <div class="row">
		<a class="btn btn-back btn-primary" href="{{ route('suppliers.index') }}"> Back</a>
	</div>

	<div class="row">
		<h3 class="header-text"> Add New Supplier </h3>
	</div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('suppliers.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

        <div class="row">
            <div class="form-group col-md-6">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name" required focus>
				<span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
        
            <div class="form-group col-md-12">
                <strong>Company:</strong>
                <input type="text" name="company" class="form-control" placeholder="Company" required focus>
				<span class="text-danger">{{ $errors->first('company') }}</span>
            </div>
        
            <div class="form-group col-md-12">
                <strong>Address:</strong>
                <textarea class="form-control" style="height:150px" name="address" placeholder="Address" required focus></textarea>
				<span class="text-danger">{{ $errors->first('address') }}</span>
            </div>
        
            <!-- <div class="form-group col-md-12">
                <strong>Brand:</strong>
                <input type="number" name="brand_id" class="form-control" placeholder="Brand">
            </div> -->
        
        <button type="submit" class="btn btn-add btn-success col-sm-2">Add</button>
        </div>
    </form>
</div>

@endsection
