@extends('layouts.main')

@section('content')
<head>
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
</head>
<style>
	.btn.btn-back {
		margin-top: 20px;
		margin-bottom: 15px;
		margin-left: 20px;
	}
	
	.header-text {
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 5px;
		font-size: 30px;
		font-family: 'Raleway', sans-serif;
		font-weight: bold;
	}

	.btn-update { 
		margin-left: 13px;
        margin-bottom: 40px;
	}

    hr { 
        margin-bottom: 20px;
    }

</style>

<div class="container">
    <div class="row">
		<a class="btn btn-back btn-primary" href="{{ route('suppliers.index') }}"> Back</a>
	</div>

	<div class="row">
		<h3 class="header-text"> Edit Supplier </h3>
	</div>

    <hr>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('suppliers.update',$supplier->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

        <div class="row">       
            <div class="form-group col-md-6">
                <strong>Name:</strong>
                <input type="text" name="name" value="{{ $supplier->name }}" class="form-control" placeholder="Name">
				<span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            
            <div class="form-group col-md-12">
                <strong>Company:</strong>
                <input type="text" name="company" value="{{ $supplier->company }}" class="form-control" placeholder="Company">
				<span class="text-danger">{{ $errors->first('company') }}</span>
            </div>
       
            <div class="form-group col-md-12">
                <strong>Address:</strong>
                <textarea class="form-control" style="height:150px" name="address" placeholder="Address">{{ $supplier->address }}</textarea>
				<span class="text-danger">{{ $errors->first('address') }}</span>
            </div>
        
            <!-- <div class="form-group col-md-12">
                <strong>Brand ID:</strong>
                <input type="number" name="brand_id" value="{{ $supplier->brand_id }}" class="form-control" placeholder="Brand ID">
            </div> -->

            <button type="submit" class="btn btn-update btn-success col-sm-2">Update</button>
        </div>
    </form> 
</div>
@endsection