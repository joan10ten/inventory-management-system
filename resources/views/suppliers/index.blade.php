@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        .btn.btn-add {
            margin-top: 10px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .btn.btn-action {
            margin-top: 5px;
            margin-bottom: 5px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 20px;
            margin-left: 35px;
            font-size: 25px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .table {
            margin-left: 20px;
            margin-right: 0;
            width: 100%;
        }

        th {
            font-family: 'Raleway', sans-serif;
        }

        td {
            word-wrap: break-word;
        }

        .alert {
            margin-left: 20px;
            max-height: 50px;
        }

        form { 
            padding-right: 20px;
        }
    </style>
</head>

<div class="row">
    <h3 class="header-text"> Manage Suppliers </h3>
</div>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-add btn-success" href="{{ route('suppliers.create') }}"> + Add New Supplier</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th scope="col" width="1%">#</th>
        <th scope="col" width="13%">Name</th>
        <th scope="col" width="20%">Company Name</th>
        <th scope="col" width="30%">Address</th>
        <th scope="col" width="15%">Brand(s)</th>
        <th scope="col" width="30%">Item(s)</th>
        <th >Action</th>
    </tr>
    @foreach ($suppliers as $supplier)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $supplier->name }}</td>
        <td>{{ $supplier->company }}</td>
        <td>{{ $supplier->address }}</td>
        <td>@foreach ( $supplier->brands as $brand) 
            <li>
                <a href={{ route('brands.show',$brand->id) }}>{{$brand->name}} </a> 
            </li>
            @endforeach
        </td>
        <td>@foreach ( $supplier->brands as $brand) @foreach($brand->products as $product)
            <li>
                <a href={{ route('products.show',$product->id) }}>{{$product->name}}</a> 
            </li>
             @endforeach @endforeach</td>
        <td>
            <form action="{{ route('suppliers.destroy',$supplier->id) }}" method="POST">
                <div class="row">
                    <a class="btn btn-action btn-primary" href="{{ route('suppliers.show',$supplier->id) }}">Show</a>
                </div>
                @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('update-supplier', $supplier))
                <div class="row">
                    <a class="btn btn-action btn-warning" href="{{ route('suppliers.edit',$supplier->id) }}">Edit</a>
                </div>
                @endif
                @if (Auth::user()->can('manage-inventory')|| Auth::user()->can('delete-supplier', $supplier))
                <div class="row">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-action btn-danger" onclick="return confirm('Are you sure? All associated brands and products with this supplier will be removed.')">Delete</button>
                </div>
                @endif
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $suppliers->links() !!}



@endsection