@extends('layouts.main')

@section('content')

<head>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
        .btn.btn-back {
            margin-top: 20px;
            margin-bottom: 15px;
            margin-left: 20px;
        }

        .header-text {
            margin-top: 10px;
            margin-left: 20px;
            margin-bottom: 5px;
            font-size: 30px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        hr {
            margin-bottom: 20px;
        }

        label {
            display: inline-block;
            width: 140px;
            text-align: left;
            font-size: 17px;
            font-family: 'Raleway', sans-serif;
            font-weight: bold;
        }

        .desc {
            padding-left: 63px;
        }

        .container {
            margin-bottom: 40px;
        }
    </style>
</head>
<div class="container">
    <div class="row">
        <a class="btn btn-back btn-primary" href="{{ route('suppliers.index') }}"> Back</a>
    </div>

    <div class="row">
        <h3 class="header-text"> Show Supplier </h3>
    </div>

    <hr>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Name</label>
            {{ $supplier->name }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Company</label>
            {{ $supplier->company }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Address</label>
            {{ $supplier->address }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label>Brand(s) Owned:</label>
            @foreach ( $supplier->brands as $brand)<li><a href={{ route('brands.show',$brand->id) }}>{{$brand->name}}</a></li>  @endforeach
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <label>Item(s) Owned:</label>
            @foreach ( $supplier->brands as $brand) @foreach($brand->products as $product)<li><a href={{ route('products.show',$product->id) }}>{{$product->name}}</a></li> @endforeach @endforeach
        </div>
    </div>
</div>
@endsection