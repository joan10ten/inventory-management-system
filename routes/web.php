<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/main', function () {
//     return view('layouts.main');
// });

// Route::get('/products/gallery', 'ProductController@gallery');
// Route::get('/brands/gallery', 'BrandController@gallery');

// Route::get('/users/editpassword', function () {
//     return view('users.editpassword');
// });

// Route::get('/products/gallery', 'ProductController@gallery');
// Route::get('/brands/gallery', 'BrandController@gallery');

// Route::resource('products', 'ProductController');
// Route::resource('suppliers', 'SupplierController');
// Route::resource('brands', 'BrandController');

Route::middleware('auth')->group(function () {
    Route::get('/products/gallery', 'ProductController@gallery')->name('products.gallery');
    Route::resource('products', 'ProductController');
    Route::resource('suppliers', 'SupplierController');
    Route::get('/brands/gallery', 'BrandController@gallery')->name('brands.gallery');
    Route::resource('brands', 'BrandController');
    Route::get('/','UserController@dashboard');
    // Route::get('/dashboard', 'UserController@dashboard');
    Route::get('/gallery', 'ProductController@galleryindex')->name('gallery.index');


});

Route::middleware(['auth', 'can:manage-users'])->group(function () {
    Route::get('/users/{user}/editpassword', 'UserController@editpassword')->name('users.editpassword');
    Route::put('/users/{user}/updatepassword', 'UserController@updatepassword')->name('users.updatepassword');
    Route::resource('users', 'UserController');
});
